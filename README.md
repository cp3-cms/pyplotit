# pyplotit: a python reimplementation of the plotIt tool

[![Documentation Status][rtd-badge]][rtd-link]

[![PyPI version][pypi-version]][pypi-link]
[![Conda-Forge][conda-badge]][conda-link]
[![PyPI platforms][pypi-platforms]][pypi-link]

This package contains a python version of much of the functionality of
the [plotIt](https://github.com/cp3-llbb/plotIt) tool, which makes
histogram stack plots, with grouping of contributions and an automatic
data/MC ratio panel below, where the relative statistical and
systematic uncertainties on each bin are shown.
It is optimised for large numbers of samples, plots and systematic
uncertainties, but is not easy to extend to e.g. several stacks (beyond
one for simulation and one for data), multiple ratios etc. - which is
what this packages tries to address.

[conda-badge]:              https://img.shields.io/conda/vn/conda-forge/plotit
[conda-link]:               https://github.com/conda-forge/plotit-feedstock
[pypi-link]:                https://pypi.org/project/plotit/
[pypi-platforms]:           https://img.shields.io/pypi/pyversions/plotit
[pypi-version]:             https://badge.fury.io/py/plotit.svg
[rtd-badge]:                https://readthedocs.org/projects/pyplotit/badge/?version=latest
[rtd-link]:                 https://pyplotit.readthedocs.io/en/latest/?badge=latest
