"""
Systematics classes (based on plotIt)
"""
__all__ = ("ParameterizedSystVar", "ConstantSystVar", "LogNormalSystVar", "ShapeSystVar", "parseSystematic")

from functools import partial
from typing import Callable, Dict, List, Optional, Union

import numpy as np

from . import config
from .logging import logger
from .plotit import BaseHist as Hist
from .plotit import lazyload  # functools or local fallback
from .plotit import FileHist, SystVar


class ParameterizedSystVar(SystVar):
    """ base for constant etc. """

    def __init__(
        self,
        name: str,
        pretty_name: Optional[str] = None,
        on: Callable[[str, config.File], bool] = SystVar.default_filter,
    ) -> None:
        super().__init__(name, pretty_name=pretty_name, on=on)

    def nom(self, hist: Hist) -> np.ndarray:
        return hist.contents

    def up(self, hist: Hist) -> np.ndarray:
        pass

    def down(self, hist: Hist) -> np.ndarray:
        pass

    class ForHist(SystVar.ForHist):
        """ delegate everything to the corresponding systVar methods """

        systVar: "ParameterizedSystVar"

        def __init__(self, hist: Hist, systVar: "ParameterizedSystVar") -> None:
            super(ParameterizedSystVar.ForHist, self).__init__(hist, systVar)

        @property
        def nom(self) -> np.ndarray:
            return self.systVar.nom(self.hist)

        @property
        def up(self) -> np.ndarray:
            return self.systVar.up(self.hist)

        @property
        def down(self) -> np.ndarray:
            return self.systVar.down(self.hist)


class ConstantSystVar(ParameterizedSystVar):
    """ constant scale up/down (given as the relative up variation, e.g. 1.2 for 20%) """

    def __init__(
        self,
        name: str,
        value: float,
        pretty_name: Optional[str] = None,
        on: Callable[[str, config.File], bool] = SystVar.default_filter,
    ) -> None:
        self.value = value
        super().__init__(name, pretty_name=pretty_name, on=on)

    def _repr_args(self) -> List[str]:
        return [self.name, str(self.value)]

    def up(self, hist: Hist) -> np.ndarray:
        return hist.contents * self.value

    def down(self, hist: Hist) -> np.ndarray:
        return hist.contents * (2 - self.value)


class LogNormalSystVar(ParameterizedSystVar):
    """ """

    def __init__(
        self,
        name: str,
        prior: float,
        postfit: float = 0.0,
        postfit_error: Optional[float] = None,
        postfit_error_up: Optional[float] = None,
        postfit_error_down: Optional[float] = None,
        pretty_name: Optional[str] = None,
        on: Callable[[str, config.File], bool] = SystVar.default_filter,
    ):
        if postfit_error_up is None:
            if postfit_error is not None:
                postfit_error_up = postfit_error
            else:
                postfit_error_up = 1.0
        if postfit_error_down is None:
            if postfit_error is not None:
                postfit_error_down = postfit_error
            else:
                postfit_error_down = 1.0
        # eval
        import math

        self.value = math.exp(postfit * math.log(prior))
        self.value_up = math.exp((postfit + postfit_error_up) * math.log(prior))
        self.value_down = math.exp((postfit - postfit_error_down) * math.log(prior))
        super().__init__(name, pretty_name=pretty_name, on=on)

    # TODO __repr__

    def up(self, hist: Hist) -> np.ndarray:
        return hist.contents * self.value_up

    def down(self, hist: Hist) -> np.ndarray:
        return hist.contents * self.value_down


class ShapeSystVar(SystVar):
    """ for shapes """

    def __init__(
        self,
        name: str,
        pretty_name: Optional[str] = None,
        on: Callable[[str, config.File], bool] = SystVar.default_filter,
    ) -> None:
        self.name = name
        super().__init__(name, pretty_name=pretty_name, on=on)

    class ForHist(SystVar.ForHist):
        hist: FileHist
        __slots__ = ("_histUp", "_histDown")

        def __init__(self, hist: FileHist, systVar: "ShapeSystVar"):
            super(ShapeSystVar.ForHist, self).__init__(hist, systVar)
            self._histUp = None
            self._histDown = None

        @lazyload
        def histUp(self) -> Hist:
            return self._findVarHist("up")

        @lazyload
        def histDown(self) -> Hist:
            return self._findVarHist("down")

        def _findVarHist(self, vari: str) -> Hist:
            variHistName = f"{self.hist.name}__{self.systVar.name}{vari}"
            if self.hist.tFile.Get(variHistName):
                return self.hist.clone(name=variHistName)
            else:  # try to find the file
                import os.path

                fullpath = self.hist.tFile.GetPath().split(":")[0]
                variPath = os.path.join(
                    os.path.dirname(fullpath),
                    f"{os.path.splitext(os.path.basename(fullpath))[0]}__{self.systVar.name}{vari}.root",
                )
                if os.path.exists(variPath):
                    from cppyy import gbl

                    vf = gbl.TFile.Open(variPath)
                    if (not vf) or vf.IsZombie() or (not vf.IsOpen()):
                        raise OSError(f"Could not open file '{variPath}' correctly")
                    if vf.Get(self.hist.name):
                        return self.hist.clone(tFile=vf)
                    else:
                        logger.error(f"Could not find '{self.hist.name}' in file '{variPath}'")
                        # raise KeyError()
                else:
                    pass  # fail quietly
                    # print("Path '{}' does not exist".format(variPath))
                    # raise IOError("Path '{}' does not exist".format(variPath))
                # print("Warning: could not find variation hist of {0} for {1}, assuming no variation then".format(self.hist, self.systVar.name))
                return self.hist

        @property
        def up(self) -> np.ndarray:
            return self.histUp.contents

        @property
        def down(self) -> np.ndarray:
            return self.histDown.contents


def parseSystematic(item: Union[str, Dict[str, Hist]]) -> SystVar:
    syst: SystVar

    if isinstance(item, str):
        syst = ShapeSystVar(item)
    elif isinstance(item, dict):
        if len(item) == 1:
            name, val = next(itm for itm in item.items())
            if isinstance(val, float):
                syst = ConstantSystVar(name, val)
            elif isinstance(val, dict):
                if val["type"] == "shape":
                    syst = ShapeSystVar(name)
                elif val["type"] == "const":
                    syst = ConstantSystVar(name, val["value"])
                elif val["type"] in ("lognormal", "ln"):
                    cfg = {k.replace("-", "_"): v for k, v in val.items()}
                    cfg.pop("type")
                    prior = cfg.pop("prior")
                    syst = LogNormalSystVar(name, prior, **cfg)
                if "pretty-name" in val:
                    syst.pretty_name = val["pretty-name"]
                if "on" in val or True in val:
                    if True in val:
                        logger.warning("Parsing 'True' as 'on', please explicitly quote the string in your config")
                        on_pat = val.pop(True)
                    else:
                        on_pat = val.pop("on")
                    import re

                    syst.on = partial((lambda fName, fObj, pat=None: bool(pat.match(fName))), pat=re.compile(on_pat))
        else:
            raise ValueError(f"Invalid systematics node, must be either a string or a map, found {item!r}")
    else:
        raise ValueError(f"Invalid systematics node, must be either a string or a map, found {item!r}")
    return syst
